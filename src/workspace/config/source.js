export const CUSTOMERS = [
  {
    id: Math.random(),
    name: "Stanley",
    lastname: "Santacruz",
    second_lastname: "Quintanilla",
    gender: "Male",
    birthday: "09-01-1990",
    marital_status: "Single",
    address: "San Salvador, #138",
    phone_number: "25648004",
    cell_number: "74545920",
    picture: "url",
    email: "zstan012@gmail.com",
    linkedin: "linkedin.com/me",
    facebook: "facebook/me",
    twitter: "twitter.com/me",
    instagram: "instagram.com/me",
    school_attended: "Universidad UTEC",
    nickname: "Developer 123",
    latitude: "13.46454",
    longitude: "-68.65456",
    active: true,
    is_deleted: false
  },
  {
    id: Math.random(),
    name: "Walter",
    lastname: "Martinez",
    second_lastname: "Ramirez",
    gender: "Male",
    birthday: "19-07-1990",
    marital_status: "Married",
    address: "San Salvador, #138",
    phone_number: "29800554",
    cell_number: "71455584",
    picture: "url",
    email: "z99sio@gmail.com",
    linkedin: "linkedin.com/you",
    facebook: "facebook/you",
    twitter: "twitter.com/you",
    instagram: "instagram.com/you",
    school_attended: "Universidad UES",
    nickname: "Peluche",
    latitude: "13.4848",
    longitude: "-68.0956",
    active: true,
    is_deleted: false
  },
  {
    id: Math.random(),
    name: "Oscar Ricardo",
    lastname: "Sosa",
    second_lastname: "Marquez",
    gender: "Male",
    birthday: "19-07-1985",
    marital_status: "Widow",
    address: "San Salvador, #64",
    phone_number: "2164455",
    cell_number: "7165456",
    picture: "url",
    email: "uiu@gmail.com",
    linkedin: "linkedin.com/he",
    facebook: "facebook/he",
    twitter: "twitter.com/he",
    instagram: "instagram.com/he",
    school_attended: "Universidad UFG",
    nickname: "Ko9i",
    latitude: "13.0895",
    longitude: "-68.0455",
    active: true,
    is_deleted: false
  },

  {
    id: Math.random(),
    name: "Alexandra",
    lastname: "Alegria",
    second_lastname: "Sanchez",
    gender: "Female",
    birthday: "10-02-1983",
    marital_status: "Married",
    address: "San Salvador, #456",
    phone_number: "2654588",
    cell_number: "70558036",
    picture: "url",
    email: "xlkss@gmail.com",
    linkedin: "linkedin.com/she",
    facebook: "facebook/she",
    twitter: "twitter.com/she",
    instagram: "instagram.com/she",
    school_attended: "Universidad UNIVO",
    nickname: "Peluche",
    latitude: "13.0845",
    longitude: "-68.7098",
    active: true,
    is_deleted: false
  },
  {
    id: Math.random(),
    name: "Juan Miguel",
    lastname: "Mata",
    second_lastname: "Polanco",
    gender: "Male",
    birthday: "12-12-1996",
    marital_status: "Single",
    address: "San Salvador, #30",
    phone_number: "2232556",
    cell_number: "74654456",
    picture: "url",
    email: "z99sio@gmail.com",
    linkedin: "linkedin.com/it",
    facebook: "facebook/it",
    twitter: "twitter.com/it",
    instagram: "instagram.com/it",
    school_attended: "Universidad UGD",
    nickname: "Lol",
    latitude: "13.4848",
    longitude: "-68.0956",
    active: true,
    is_deleted: false
  },

  {
    id: Math.random(),
    name: "Irvin",
    lastname: "Bordowsky",
    second_lastname: "Mata",
    gender: "Male",
    birthday: "19-08-1988",
    marital_status: "Married",
    address: "San Salvador, #18",
    phone_number: "29581578",
    cell_number: "79658956",
    picture: "url",
    email: "akso@gmail.com",
    linkedin: "linkedin.com/they",
    facebook: "facebook/they",
    twitter: "twitter.com/they",
    instagram: "instagram.com/they",
    school_attended: "Universidad Hardvard",
    nickname: "Kosd",
    latitude: "13.050",
    longitude: "-68.0956",
    active: true,
    is_deleted: false
  }
];

export const FIELD_SECTIONS = [
  {
    id: 1,
    name: "Basic information",
    isActive: true
  },
  {
    id: 2,
    name: "Configuration",
    isActive: true
  },
  {
    id: 3,
    name: "Business information",
    isActive: true
  },
  {
    id: 4,
    name: "Finance information",
    isActive: true
  },
  {
    id: 5,
    name: "Others",
    isActive: true
  },
  {
    id: 6,
    name: "Medic information",
    isActive: true
  },
  {
    id: 7,
    name: "Product list",
    isActive: false
  }
];

export const FIELDS = [
  {
    name: "name",
    isVisible: true,
    label: "Name",
    type: "string",
    isRequired: true,
    section_id: 1
  },
  {
    name: "lastname",
    isVisible: true,
    label: "Last name",
    type: "string",
    isRequired: true,
    section_id: 1
  },
  {
    name: "second_lastname",
    isVisible: true,
    label: "Second lastname",
    type: "string",
    isRequired: false,
    section_id: 1
  },
  {
    name: "gender",
    isVisible: true,
    label: "Gender",
    type: "pick-list",
    array: ["Male", "Female"],
    isRequired: true,
    section_id: 1
  },
  {
    name: "birthday",
    isVisible: true,
    label: "Birthday",
    type: "date",
    isRequired: true,
    section_id: 1
  },
  {
    name: "marital_status",
    isVisible: true,
    label: "Marital Status",
    type: "pick-list",
    array: ["Single", "Married", "Divorced", "Widow", "Other"],
    isRequired: false,
    section_id: 1
  },
  {
    name: "address",
    isVisible: true,
    label: "Address",
    type: "text",
    isRequired: false,
    section_id: 1
  },
  {
    name: "phone_number",
    isVisible: true,
    label: "Phone number",
    type: "string",
    isRequired: false,
    section_id: 1
  },
  {
    name: "cell_number",
    isVisible: true,
    label: "Cell number",
    type: "string",
    isRequired: false,
    section_id: 1
  },
  {
    name: "picture",
    isVisible: true,
    label: "Picture",
    type: "img",
    isRequired: false,
    section_id: 1
  },
  {
    name: "email",
    isVisible: true,
    label: "Email",
    type: "string",
    isRequired: true,
    section_id: 1
  },
  {
    name: "linkedin",
    isVisible: true,
    label: "LinkedIn Profile URL",
    type: "url",
    isRequired: false,
    section_id: 1
  },
  {
    name: "facebook",
    isVisible: true,
    label: "Facebook Profile URL",
    type: "url",
    isRequired: false,
    section_id: 1
  },
  {
    name: "twitter",
    isVisible: true,
    label: "Twitter Handle",
    type: "url",
    isRequired: false,
    section_id: 1
  },
  {
    name: "instagram",
    isVisible: true,
    label: "Instagram Tag",
    type: "url",
    isRequired: false,
    section_id: 1
  },
  {
    name: "school_attended",
    isVisible: true,
    label: "School Attended",
    type: "text",
    isRequired: false,
    section_id: 1
  },
  {
    name: "nickname",
    isVisible: true,
    label: "Nickname",
    type: "short_string",
    isRequired: false,
    section_id: 1
  },
  {
    name: "latitude",
    isVisible: true,
    label: "Latitude",
    type: "numeric",
    isRequired: false,
    section_id: 1
  },
  {
    name: "longitude",
    isVisible: true,
    label: "Longitude",
    type: "numeric",
    isRequired: false,
    section_id: 1
  },
  {
    name: "active",
    isVisible: true,
    label: "Active",
    type: "boolean",
    isRequired: false,
    section_id: 2
  },
  {
    name: "business_name",
    isVisible: true,
    label: "Business name",
    type: "text",
    isRequired: false,
    section_id: 3
  },
  {
    name: "employment_status",
    isVisible: true,
    label: "Employment Status",
    type: "pick-list",
    array: ["None", "Working", "Employeer", "Fired"],
    isRequired: false,
    section_id: 3
  },
  {
    name: "job_type",
    isVisible: true,
    label: "Job Type",
    type: "string",
    isRequired: false,
    section_id: 3
  },
  {
    name: "salary",
    isVisible: true,
    label: "Salary",
    type: "numeric",
    isRequired: false,
    section_id: 3
  },
  {
    name: "work_address",
    isVisible: true,
    label: "Work Address",
    type: "text",
    isRequired: false,
    section_id: 3
  },
  {
    name: "company_phone",
    isVisible: true,
    label: "Company phone",
    type: "string",
    isRequired: true,
    section_id: 3
  },
  {
    name: "department",
    isVisible: true,
    label: "Department",
    type: "string",
    isRequired: false,
    section_id: 3
  },
  {
    name: "preferred_contact_time",
    isVisible: true,
    label: "Preferred Contact Time",
    type: "pick-list",
    array: ["Morning", "Afternoon", "Nigth"],
    isRequired: false,
    section_id: 3
  },
  {
    name: "preferred_contact_day",
    isVisible: true,
    label: "Preferred Contact Day",
    type: "pick-list",
    array: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
    isRequired: false,
    section_id: 3
  },
  {
    name: "professional_accreditation",
    isVisible: true,
    label: "Professional accreditation",
    type: "boolean",
    isRequired: false,
    section_id: 3
  },
  {
    name: "willingness_to_travel",
    isVisible: true,
    label: "Willingness to travel",
    type: "pick-list",
    array: ["Full", "Partial", "None"],
    isRequired: false,
    section_id: 3
  },
  {
    name: "transport_medium",
    isVisible: true,
    label: "Transport medium",
    type: "string",
    isRequired: false,
    section_id: 3
  },
  {
    name: "credit_limit",
    isVisible: true,
    label: "Credit Limit",
    type: "string",
    isRequired: false,
    section_id: 4
  },
  {
    name: "payment_terms",
    isVisible: true,
    label: "Payment Terms",
    type: "text",
    isRequired: false,
    section_id: 4
  },
  {
    name: "score",
    isVisible: true,
    label: "Score",
    type: "numeric",
    isRequired: false,
    section_id: 4
  },
  {
    name: "client_category",
    isVisible: true,
    label: "Client category",
    type: "string",
    isRequired: false,
    section_id: 4
  },
  {
    name: "loan_amount",
    isVisible: true,
    label: "Loan Amount",
    type: "numeric",
    isRequired: false,
    section_id: 4
  },
  {
    name: "last_donation_amount",
    isVisible: true,
    label: "Last Donation Amount",
    type: "numeric",
    isRequired: false,
    section_id: 5
  },
  {
    name: "last_donation_date",
    isVisible: true,
    label: "Last Donation Date",
    type: "date",
    isRequired: false,
    section_id: 5
  },
  {
    name: "membership_expiry_date",
    isVisible: true,
    label: "Membership Expiry Date",
    type: "date",
    isRequired: false,
    section_id: 5
  },
  {
    name: "alergic_to",
    isVisible: true,
    label: "Alergic to",
    type: "text",
    isRequired: false,
    section_id: 6
  },
  {
    name: "last_period",
    isVisible: true,
    label: "Last period",
    type: "date",
    isRequired: false,
    section_id: 6
  },
  {
    name: "has_children",
    isVisible: true,
    label: "Has children",
    type: "boolean",
    isRequired: false,
    section_id: 6
  },
  {
    name: "vaccines",
    isVisible: true,
    label: "Vaccines",
    type: "string",
    isRequired: false,
    section_id: 6
  },
  {
    name: "blood_type",
    isVisible: true,
    label: "Blood type",
    type: "short_string",
    isRequired: false,
    section_id: 6
  }
];
