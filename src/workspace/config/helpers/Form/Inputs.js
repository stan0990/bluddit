import React from "react";
import {
  InputLabel,
  Select,
  MenuItem,
  FormControl,
  Input,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import Check from "@material-ui/icons/Check";

import { styles } from "./styles";

// Helper to render inputs depending de prop defined

export const inputType = (field, onChange, value, devVisible) => {
  switch (field.type) {
    case "string":
    case "url":
      return (
        <FormControl style={styles.customFormControl} fullWidth>
          <InputLabel>{field.label}</InputLabel>
          <Input
            labelText={field.label}
            name={field.name}
            onChange={onChange}
            value={value}
            readOnly={devVisible}
            success={true}
            error={false}
          />
        </FormControl>
      );

    case "text":
      return (
        <FormControl style={styles.customFormControl} fullWidth>
          <InputLabel>{field.label}</InputLabel>
          <Input
            labelText={field.label}
            name={field.name}
            onChange={onChange}
            value={value}
            readOnly={devVisible}
            multiline
            rows={5}
          />
        </FormControl>
      );

    case "pick-list":
      return (
        <FormControl fullWidth style={styles.customFormControl}>
          <InputLabel>{field.label}</InputLabel>

          <Select name={field.name} value={value} onClick={onChange}>
            {field.array !== undefined &&
              field.array.length > 0 &&
              field.array.map(item => (
                <MenuItem value={item} key={item}>
                  {item}
                </MenuItem>
              ))}
          </Select>
        </FormControl>
      );
    case "img":
      return (
        <div style={styles.customFormControl}>
          <InputLabel>{field.label}</InputLabel>
          <input type="file" onChange={onChange} name={field.name} />
          {value}
        </div>
      );

    case "date":
      return (
        <FormControl fullWidth style={styles.customFormControl}>
          <InputLabel>{field.label}</InputLabel>
          <br />
          <br />
          <Input
            labelText={field.label}
            name={field.name}
            onChange={onChange}
            value={value}
            readOnly={devVisible}
            type="date"
          />
        </FormControl>
      );

    case "short_string":
    case "numeric":
      return (
        <FormControl style={styles.customFormControl}>
          <InputLabel>{field.label}</InputLabel>
          <Input
            labelText={field.label}
            name={field.name}
            onChange={onChange}
            value={value}
            readOnly={devVisible}
          />
        </FormControl>
      );
    case "boolean":
      return (
        <FormControlLabel
          control={
            <Checkbox
              name={field.name}
              tabIndex={-1}
              onClick={onChange}
              checkedIcon={<Check className={"create-customer-checked-icon"} />}
              icon={<Check className={"create-customer-unchecked-icon"} />}
            />
          }
          checked={value}
          disabled={devVisible}
          label={field.label}
        />
      );

    default:
      return (
        <FormControl fullWidth style={styles.customFormControl}>
          {field.name}
        </FormControl>
      );
  }
};
