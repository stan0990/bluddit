import React from "react";
import { inputType } from "./Inputs";
import GridItem from "components/Grid/GridItem";
import { FormControlLabel, Checkbox } from "@material-ui/core";
import Check from "@material-ui/icons/Check";

// eslint-disable-next-line react/display-name
export default ({
  field,
  arrayOptions,
  devVisible,
  onChange,
  onChangeChecked,
  value
}) => {
  return devVisible ? (
    <>
      <GridItem xs={12} sm={8}>
        {inputType(field, onChange, value, devVisible)}
      </GridItem>
      <GridItem xs={12} sm={2}>
        {devVisible && field.type !== "section" && (
          <FormControlLabel
            className="create-customer-check-wrapper"
            control={
              <Checkbox
                tabIndex={-1}
                onClick={() => onChangeChecked(field.name)}
                checkedIcon={
                  <Check className={"create-customer-checked-icon"} />
                }
                icon={<Check className={"create-customer-unchecked-icon"} />}
              />
            }
            checked={field.isVisible}
            disabled={field.isRequired}
            label="Visible"
          />
        )}
      </GridItem>
    </>
  ) : field.isVisible ? (
    <GridItem xs={12} sm={8}>
      {inputType(field, onChange, value)}
    </GridItem>
  ) : (
    <div></div>
  );
};
