import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Check from "@material-ui/icons/Check";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import Save from "@material-ui/icons/Save";

import "../../../config/assets/custom-styles.css";
import Field from "./Field";
import Card from "components/Card/Card";
import CardHeader from "components/Card/CardHeader";
import CardText from "components/Card/CardText";
import CardBody from "components/Card/CardBody";
import GridContainer from "components/Grid/GridContainer";
import { Button } from "@material-ui/core";
//import Field from "./Field";

export class Form extends React.Component {
  state = { devMode: false };

  onChangeDevMode = () => {
    //Switching dev Mode boolean value
    const { devMode } = this.state;
    this.setState({ devMode: !devMode });
  };

  render() {
    const {
      fields,
      onChangeInputValue,
      onChangeChecked,
      form,
      sendForm
    } = this.props;
    const { devMode } = this.state;

    return (
      <>
        <FormControlLabel
          control={
            <Checkbox
              tabIndex={-1}
              onClick={this.onChangeDevMode}
              checkedIcon={<Check className={"create-customer-checked-icon"} />}
              icon={<Check className={"create-customer-unchecked-icon"} />}
            />
          }
          label="Developer mode"
        />
        <Card>
          <CardHeader color="info" text>
            <CardText color="info">
              <h4>Create a new customer</h4>
            </CardText>
          </CardHeader>
          <CardBody>
            <GridContainer>
              {fields !== undefined ? (
                fields.map((field, index) => (
                  <Field
                    key={index}
                    field={field}
                    devVisible={devMode}
                    onChange={onChangeInputValue}
                    onChangeChecked={onChangeChecked}
                    value={form !== undefined ? form[field.name] : undefined}
                  />
                ))
              ) : (
                <div></div>
              )}
            </GridContainer>
            <Button
              className="create-customer-button-add"
              onClick={sendForm}
              disabled={devMode}
            >
              <Save /> Save Customer
            </Button>

            <Button
              className="create-customer-button-back"
              onClick={() => (window.location.href = "/admin/customers")}
            >
              <KeyboardArrowLeft /> Cancel
            </Button>
          </CardBody>
        </Card>
      </>
    );
  }
}
