import React, { Component } from "react";
import { Link } from "react-router-dom";

import { getData, storeData } from "../../../config/helpers/useLocalStorage";
import { FIELDS_LIST, CUSTOMERS_LIST } from "../../../config/constants";
import { Form } from "workspace/config/helpers/Form";

/* For more details see: components/customers/Create/container.ts has almost the 
same functions with this component.
This component will edit a customer but in different screen */

export class EditCustomer extends Component {
  state = {
    fields: [],
    form: {}
  };

  componentDidMount() {
    this.fetchFields();
    this.loadData();
  }

  fetchFields = () => {
    let fields = getData(FIELDS_LIST);
    this.setState({ fields });
  };

  loadData = () => {
    const path = window.location.pathname;
    const currentCustomer = path.substring(path.lastIndexOf("/") + 1);
    let customers = getData(CUSTOMERS_LIST);
    const signgleCustomer = customers.find(
      customer => customer.id === Number(currentCustomer)
    );
    this.setState({ form: signgleCustomer });
  };

  onChangeInputValue = e => {
    const { form } = this.state;
    const formValues = { ...form, [e.target.name]: e.target.value };
    this.setState({ form: formValues });
  };

  onChangeChecked = name => {
    const { fields } = this.state;
    const newFields = fields.map(field => {
      if (field.name === name) {
        field = {
          ...field,
          isVisible: !field.isVisible
        };
      }
      return field;
    });

    this.setState({ fields: newFields });
    storeData(FIELDS_LIST, newFields);
  };

  sendForm = e => {
    const { form } = this.state;
    const formState = form;
    e.preventDefault();
    const currentID = formState.id;
    let customers = getData(CUSTOMERS_LIST);
    const customerIndex = customers.findIndex(
      customer => customer.id === Number(currentID)
    );
    customers[customerIndex] = form;
    storeData(CUSTOMERS_LIST, customers);
    window.location.href = "/admin/customers";
  };

  render() {
    const { fields, form } = this.state;
    return (
      <div className="AppWrapper">
        <form className="form">
          <Form
            fields={fields}
            form={form}
            onChangeInputValue={this.onChangeInputValue}
            onChangeChecked={this.onChangeChecked}
            sendForm={this.sendForm}
          />
          <Link to="/">Cancelar</Link>
        </form>
      </div>
    );
  }
}
