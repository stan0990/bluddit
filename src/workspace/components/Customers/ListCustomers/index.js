import React from "react";
import ReactTable from "react-table";
import Button from "components/CustomButtons/Button.js";
import Edit from "@material-ui/icons/Edit";
import Archive from "@material-ui/icons/Archive";
import { Link } from "react-router-dom";

export default function LisCustomers({ customers, onArchive }) {
  const data = customers.map(prop => {
    return {
      fullName: `${prop.name} ${prop.lastname}`,
      phone: `${prop.phone_number}/${prop.cell_number}`,
      email: prop.email,
      gender: prop.gender,
      nickname: prop.nickname,
      actions: (
        <div>
          <Link
            to={`/admin/customers/edit/${prop.id}`}
            className="customers-list-edit"
          >
            <Edit />
          </Link>{" "}
          <Button
            justIcon
            round
            simple
            color="danger"
            className="like"
            onClick={() => onArchive(prop.id)}
          >
            <Archive />
          </Button>{" "}
        </div>
      )
    };
  });

  return (
    <ReactTable
      defaultPageSize={10}
      filterable
      className="-striped -highlight"
      columns={[
        { Header: "Full name", accessor: "fullName" },
        { Header: "Phone", accessor: "phone" },
        { Header: "Email", accessor: "email" },
        { Header: "Gender", accessor: "gender" },
        { Header: "Email", accessor: "email" },
        { Header: "Nickname", accessor: "nickname" },
        {
          Header: "",
          accessor: "actions",
          sortable: false,
          filterable: false
        }
      ]}
      data={data}
    />
  );
}
