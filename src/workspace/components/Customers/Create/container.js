import React, { Component } from "react";
import { Link } from "react-router-dom";

import { getData, storeData } from "../../../config/helpers/useLocalStorage";
import { FIELDS_LIST, CUSTOMERS_LIST } from "../../../config/constants";
import { Form } from "workspace/config/helpers/Form";

export class CreateCustomer extends Component {
  state = {
    fields: [],
    form: {}
  };

  componentDidMount() {
    this.fetchFields();
  }

  fetchFields = () => {
    let fields = getData(FIELDS_LIST);
    this.setState({ fields });
    console.log(this.state.fields);
    // Fetching fields from localStorage, after that we will show them in screen
  };

  onChangeInputValue = e => {
    const { form } = this.state;
    const formValues = { ...form, [e.target.name]: e.target.value };
    this.setState({ form: formValues });
    /* We use an object here for form values,
    for example: 
    *this.state.form*, will return all form values, instead using some like this:
    *this.state.form.name*
    *this.state.form.lastname* etc...

    *formValues* loads dynamicly to state in order to send to backend */
  };

  onChangeChecked = name => {
    // Function to configure fields in developer mode.
    const { fields } = this.state;
    const newFields = fields.map(field => {
      if (field.name === name) {
        field = {
          ...field,
          isVisible: !field.isVisible
        };
      }
      return field;
    });

    this.setState({ fields: newFields });
    storeData(FIELDS_LIST, newFields);
  };

  sendForm = e => {
    const { form } = this.state;
    // *form* will return all form customer values
    e.preventDefault();
    console.log(form);
    let data = getData(CUSTOMERS_LIST);
    data.push({ ...form, id: Math.random(), is_deleted: false });
    storeData(CUSTOMERS_LIST, data);
    window.location.href = "/admin/customers";
  };

  render() {
    const { fields } = this.state;
    return (
      <div className="AppWrapper">
        <form className="form">
          <Form
            fields={fields}
            onChangeInputValue={this.onChangeInputValue}
            onChangeChecked={this.onChangeChecked}
            sendForm={this.sendForm}
          />
          <Link to="/admin/customers">Cancelar</Link>
        </form>
      </div>
    );
  }
}
